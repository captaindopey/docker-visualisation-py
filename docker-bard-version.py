import docker


class Node:
    def __init__(self, value, image_id, command, history, children=[]):
        self.value = value
        self.image_id = image_id
        self.command = command
        self.history = history
        self.children = children

    def __repr__(self):
        return "+ {} ({}): {}{}\n{}".format(
            self.value,
            self.image_id,
            self.command,
            "\n".join(self.history),
            "\n".join(repr(child) for child in self.children),
        )


def generate_tree_string(node):
    if not node.children:
        return str(node.value)
    else:
        return "+ {} ({}): {} {}\n{}".format(
            node.value,
            node.image_id,
            node.command,
            "\n".join(node.history),
            "\n".join(generate_tree_string(child) for child in node.children),
        )


def compare_images(images):
    """Compares a list of Docker images.

    Args:
      images: A list of Docker image names.

    Returns:
      A dictionary of layer IDs where the images diverge, or None if the images do not diverge.
    """

    # Get the layer IDs of each image.
    image_layer_ids = {}
    for image in images:
        image_layer_ids[image] = (
            docker.from_env().images.get(image).attrs["RootFS"]["Layers"]
        )

    # Compare the layer IDs of the images.
    divergence_layer_ids = {}
    for i in range(len(image_layer_ids[images[0]])):
        for image in images[1:]:
            if image_layer_ids[images[0]][i] != image_layer_ids[image][i]:
                divergence_layer_ids[i] = image_layer_ids[images[0]][i]
                break

    # The images do not diverge.
    if not divergence_layer_ids:
        return None

    return divergence_layer_ids


# Example usage:

repositoryName = "cs-course-tools"
versions = ["0.0.1", "0.0.2", "0.0.3", "0.0.4", "0.0.5"]

image_tags = list(map(lambda x: repositoryName + ":" + x, versions))

divergence_layer_ids = compare_images(image_tags)

if divergence_layer_ids is not None:
    # Create a root node for the tree.
    root_node = Node("Divergence layer IDs", None, None, [], [])

    # Add a child node for each layer ID where the images diverge.
    for layer_id in divergence_layer_ids:
        command = docker.from_env().images.get(layer_id).attrs["History"][0]["Cmd"]
        history = docker.from_env().images.get(layer_id).attrs["History"]
        root_node.children.append(Node(layer_id, layer_id, command, history))

    # Generate the string representation of the tree.
    tree_string = generate_tree_string(root_node)

    # Print the tree string.
    print(tree_string)
else:
    print("The images do not diverge.")
