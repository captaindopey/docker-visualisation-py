import docker
from typing import List, Dict, Any, Tuple

class ImageLayer:
    def __init__(self, layer_id: str, size: int, created_by: str):
        self.layer_id: str = layer_id
        self.size: int = size
        self.created_by: int = created_by
        self.next_layers: List['ImageLayer'] = []
        self.prev_layer: 'ImageLayer' = None

    def add_next_layer(self, next_layer: 'ImageLayer'):
        self.next_layers.append(next_layer)
        next_layer.prev_layer = self

    def __str__(self):
        return self.pretty_print()

    def pretty_print(self, indent: str = '') -> str:
        layer_desc = self.created_by[:30] if self.layer_id == '<missing>' else self.layer_id[:30]
        result = f"{indent}Layer Id: {self.layer_id}, Size: {self.size}, Layer Desc: {layer_desc}\n"
        many_children = len(self.next_layers) > 1
        for i, child in enumerate(self.next_layers):
            child_indent = indent + ('  │  ' if many_children else '')
            result += child.pretty_print(child_indent)
        return result

def get_image_layers(image_name: str) -> List[Dict[str, Any]]:
    client = docker.from_env()
    image = client.images.get(image_name)
    layers = []

    for layer_id in image.attrs["RootFS"]["Layers"]:
        layer_info = client.api.inspect_layer(layer_id)
        layers.append(layer_info)

    return layers

def build_image_layer_list(image_history: List[Dict[str, Any]]) -> ImageLayer:
    head = None
    prev_layer = None
    layer_map = {}  # Map layer IDs to their corresponding ImageLayer objects

    for layer_data in image_history:
        layer_id = layer_data['Id']
        layer_size = layer_data['Size']
        created_by = layer_data['CreatedBy']

        layer = ImageLayer(layer_id, layer_size, created_by)
        layer_map[layer_id] = layer

        if prev_layer:
            prev_layer.add_next_layer(layer)
        else:
            head = layer  # The first layer becomes the head of the linked list

        prev_layer = layer

    return head

def compare_image_layers(layers1: ImageLayer, layers2: ImageLayer) -> List[Tuple[ImageLayer, ImageLayer, List[str]]]:
    differences: List[Tuple[ImageLayer, ImageLayer, List[str]]] = []
    layer1 = layers1
    layer2 = layers2

    while layer1 and layer2:
        if layer1.layer_id != layer2.layer_id:
            diff_lines: List[str] = [f"Layer ID: {layer1.layer_id} ({layer1.size}) != {layer2.layer_id} ({layer2.size})"]
            differences.append((layer1, layer2, diff_lines))
            break  # Stop tracking further layers after the first divergence
        layer1 = layer1.next_layers[0] if layer1.next_layers else None
        layer2 = layer2.next_layers[0] if layer2.next_layers else None

    return differences

def compare_images(images: List[str]) -> Dict[str, Dict[str, List[ImageLayer]]]:
    image_layers_map = {}

    for image_name in images:
        image_history = get_image_layers(image_name)
        image_layer_list = build_image_layer_list(image_history)
        image_layers_map[image_name] = image_layer_list

    image_differences = {}

    for i, image_name1 in enumerate(images):
        for j, image_name2 in enumerate(images):
            if i < j:
                differences = compare_image_layers(image_layers_map[image_name1], image_layers_map[image_name2])
                if differences:
                    if image_name1 not in image_differences:
                        image_differences[image_name1] = {}
                    image_differences[image_name1][image_name2] = differences

    return image_differences


def squash_results_into_root_layers(image_differences: Dict[str, Dict[str, List[ImageLayer]]]) -> List[ImageLayer]:
    # Create a set to store all layers
    all_layers = set()

    # Add all layers from the image differences to the set
    for comparisons in image_differences.values():
        for differences in comparisons.values():
            for layer1, layer2, _ in differences:
                all_layers.add(layer1)
                all_layers.add(layer2)

    # Create a list of root layers
    root_layers = []
    for layer in all_layers:
        # If a layer has no previous layers, it's a root layer
        if not layer.prev_layer:
            root_layers.append(layer)

    return root_layers

if __name__ == "__main__":
    repositoryName='cs-course-tools'
    versions=['0.0.1', '0.0.2', '0.0.3', '0.0.4', '0.0.5']

    image_tags=list(map(lambda x : repositoryName + ':' + x, versions))

    image_differences = compare_images(image_tags)
    root_layers = squash_results_into_root_layers(image_differences)

    if root_layers:
      print("Root Layers:")
      for i, root_layer in enumerate(root_layers):
          print(root_layer.pretty_print(''))  # Use the pretty_print method for pretty printing

    else:
        print("No root layers found.")