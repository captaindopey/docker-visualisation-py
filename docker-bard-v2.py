from typing import List

import docker

client = docker.from_env()

def determine_if_images_have_different_layer_ids(image1: docker.models.images.Image, image2: docker.models.images.Image) -> bool:
    """
    Determine if two images have different layer IDs.

    Args:
        image1: The first image to compare.
        image2: The second image to compare.

    Returns:
        True if the images have different layer IDs, False otherwise.
    """

    image1_layer_ids: List[str] = []
    for layer in image1.history():
        image1_layer_ids.append(layer["Id"])

    image2_layer_ids: List[str] = []
    for layer in image2.history():
        image2_layer_ids.append(layer["Id"])

    return image1_layer_ids != image2_layer_ids

if __name__ == '__main__':
    image1: docker.models.images.Image = client.images.get('cs-course-tools:0.0.1')
    image2: docker.models.images.Image = client.images.get('cs-course-tools:0.0.2')

    if determine_if_images_have_different_layer_ids(image1, image2):
        print('The images have different layer IDs.')
    else:
        print('The images have the same layer IDs.')