import requests
import json

class Node:
    def __init__(self, sha, children) -> None:
        self.sha = sha
        self.children = children


def searchList(list: list, func):
    for i in range(len(list)):
        if func(list[i]):
            return list[i]
    return None
        
def orderedHistory(list: list, func, result: list) -> None:
    item = searchList(list, func)
    if not item is None:
        result.append(item)
        orderedHistory(list, (lambda i: i.get('parent', '-') == item['id']), result)

def buildList(request) -> list:
    historyArray = list(map(lambda item: json.loads(item['v1Compatibility']), request.json()['history']))
    result = []
    orderedHistory(historyArray, lambda item: not 'parent' in item, result)
    return (request, result)


registryUrl='http://host.docker.internal:5000'
repositoryName='hello-world-simple'
versions=['1.0.0', '1.0.1']

responseManifest = map(lambda version: requests.get(registryUrl + '/v2/' + repositoryName + '/manifests/' + version), versions)

requestsWithHistory = map(lambda request: buildList(request), responseManifest)

for request, history in list(requestsWithHistory):
    print(request)
    print(len(list(history)))
